<?php

use Illuminate\Support\Facades\Route;
use Modules\Auth\Http\Controllers\AuthController;
use Modules\Auth\Http\Controllers\PasswordResetController;
use Modules\Auth\Http\Controllers\ChangePasswordController;
use Modules\Auth\Http\Controllers\UserController;
use Modules\Auth\Http\Controllers\StateController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix' => 'auth', 'middleware' => ['auth.apikey', 'localization']], function ($router) {
    Route::post('/register',[AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);

    Route::get('/states', [StateController::class, 'allStates']);
    Route::get('/cities/{id}', [StateController::class, 'getCitiesByStateId']);

    Route::post('/update', [UserController::class, 'UpdateUserDetails']);
    Route::get('/profile' , [UserController::class, 'userProfile']);
    Route::post('/send-reset-link', [PasswordResetController::class, 'sendEmail']);
    Route::post('/reset-password', [ChangePasswordController::class, 'passwordResetProcess']);
});
