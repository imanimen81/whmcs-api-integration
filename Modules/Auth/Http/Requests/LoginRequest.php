<?php

namespace Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Redirect;
class LoginRequest extends FormRequest
{
    public function response(array $errors){
        return Redirect::back()->withErrors($errors)->withInput();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6'
        ];
    }
    public function messages()
    {
        return [
            'email.required' => trans('auth.EmailRequired'),
            'email.email' => trans('auth.InvalidEmail'),
            'password.required' => trans('auth.RequiredPass'),
            'password.min' => trans('message.PasswordMin')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
