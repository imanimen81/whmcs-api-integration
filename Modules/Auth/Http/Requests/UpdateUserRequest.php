<?php

namespace Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'bail|string|max:255',
            'lastname' => '|string|max:255',
            'email' => '|email|max:35|unique:users',
            'city' => '|string|max:255',
            'country' => '|between:0,3|regex:([A-Z])',
            'state' => '|string|max:255',
            'postcode' => '|string|max:255|unique:users',
            'phonenumber' => '|unique:users',
            'address1' => '|string|max:255',
            'password' => '|min:6|confirmed',
        ];
    }

    public function messages()
    {
        return [

            'email.email' => trans('auth.InvalidEmail'),
            "email.unique" => trans('auth.EmailExists'),
            'phonenumber.unique' => trans('auth.PhoneNumberUnique'),
            'postcode.unique' => trans('auth.postcodeUnique'),
            'password.required' => trans('auth.RequiredPass'),
            'password.confirmed' => trans('auth.ConfirmPass'),
            'password.min' => trans('message.PasswordMin'),
            'country.regex' => trans('message.CountryRegex')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
