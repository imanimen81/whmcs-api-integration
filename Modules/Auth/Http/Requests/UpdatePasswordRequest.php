<?php

namespace Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed'
        ];
    }

    public function messages(){
        return [
          'email.required' => trans('message.EmailRequired'),
            'email.email' => trans('message.EmailInvalid'),
            'password.confirmed' => trans('auth.ConfirmPass'),
            'password.min' => trans('auth.PasswordMin')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
