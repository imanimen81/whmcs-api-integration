<?php

namespace Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'bail|required|email|max:35|unique:users',
            'city' => 'required|string|max:255',
            'country' => 'required|between:0,3|regex:([A-Z])',
            'state' => 'required|string|max:255',
            'postcode' => 'required|string|max:255|unique:users',
            'phonenumber' => 'required|unique:users|min:11|numeric',
            'address1' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
        ];
    }
    public function messages(){
        return  [
            'email.required' => trans('auth.EmailRequired'),
            'email.email' => trans('auth.InvalidEmail'),
            "email.unique" => trans('auth.EmailExists'),
            'city.required' => trans('auth.CityRequired'),
            'address1.required' => trans('auth.AddrRequired'),
            'state.required' => trans('auth.StateRequired'),
            'country.required' => trans('auth.CountryRequired'),
            'phonenumber.required' => trans('auth.PhoneNumberRequired'),
            'phonenumber.unique' => trans('auth.PhoneNumberUnique'),
            'phonenumber.min' => trans('message.PhonenumberMin'),
            'phonenumber.numeric' => trans('message.phonenumberNum'),
            'postcode.unique' => trans('auth.postcodeUnique'),
            'postcode.required' => trans('auth.PostRequired'),
            'firstname.required' => trans('auth.FnameRequired'),
            'lastname.required' => trans('auth.LnameRequired'),
            'password.required' => trans('auth.RequiredPass'),
            'password.confirmed' => trans('auth.ConfirmPass'),
            'password.min' => trans('message.PasswordMin'),
            'country.regex' => trans('message.CountryRegex')
        ];
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

}
