<?php

namespace Modules\Auth\Http\Controllers;


use App\Models\City;
use App\Models\State;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Http\Requests\RegisterRequest as RegisterRequest;
use Modules\Auth\Http\Requests\LoginRequest as LoginRequest;
use Modules\Whmcs\Http\Traits\Client;
use Modules\Whmcs\Http\Traits\Connection;
use App\Models\User;
use Modules\Auth\Http\Requests\UpdateUserRequest as UpdateUserRequest;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use Client;
    use Connection;

    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }


    // login
    public function login(LoginRequest $request)
    {
        if (!$token = auth()->attempt($request->only('email', 'password'))) {
            return response()->json(
                [
                    'error' => trans('auth.Unauthorized')
                ], 401);
        } else {
            return $this->createNewToken($token);
        }
    }

    public function register(RegisterRequest $request)
    {
        $data = [
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'city' => $request->input('city'),
            'state' => $request->input('state'),
            'country' => $request->input('country'),
            'postcode' => $request->input('postcode'),
            'phonenumber' => $request->input('phonenumber'),
            'address1' => $request->input('address'),
            'password' => $request->input('password')
        ];
        $email = $request->input('email');
        $dataWhmcs = [
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'city' => $request->input('city'),
            'state' => $request->input('state'),
            'country' => $request->input('country'),
            'postcode' => $request->input('postcode'),
            'phonenumber' => $request->input('phonenumber'),
            'address1' => $request->input('address'),
            'password2' => $request->input('password')
        ];

        $user = (new User)->register($data);
        $clientId = (new User)->RegisterClient($dataWhmcs);
        $insertClientId = (new User)->updateClientId($email, $clientId);
        return $this->loginAfterRegister($request);

    }

    public function logout()
    {
        auth()->logout();
        return response()->json(
            $data = [
                'message' => trans('auth.LogedOut')
            ],409);
    }

    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }

    protected function createNewToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);


        
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse|void
     */
    protected function loginAfterRegister(RegisterRequest $request)
    {
        try {
            if ($token = auth()->attempt($request->only('email', 'password'))) {
                return response()->json([
                    'data' => [
                        'message' => 'User created successfully',
                        'user' => $this->createNewToken($token)->original,
                    ]
                ]);
            } elseif ((!$token = auth()->attempt($request->only('email', 'password')))) {
                return response()->json([
                    'error' => 'User exists in whmcs or something went wrong in whmcs'
                ], 401);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
