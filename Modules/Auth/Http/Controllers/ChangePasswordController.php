<?php

namespace Modules\Auth\Http\Controllers;


use Illuminate\Routing\Controller;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Modules\Auth\Http\Requests\UpdatePasswordRequest as UpdatePasswordRequest;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function passwordResetProcess(UpdatePasswordRequest $request){
        return $this->updatePasswordRow($request)->count() > 0 ? $this->resetPassword($request) : $this->tokenNotFoundError();
    }

    // verify token validation
    private function updatePasswordRow($request){
        return DB::table('password_resets')->where([
            'email' => $request->email,
            'token' => $request->token
        ]);
    }

    private function tokenNotFoundError(){
        return response()->json([
           'error' => 'Email or Token is wrong'
        ], 401);
    }

    public function resetPassword($request){
        $userData = User::where('email', $request->email)->first();
        $userData->update([
            'password' => Hash::make($request->password)
        ]);
        $this->updatePasswordRow($request)->delete();

        return response()->json([
            'data' => 'Password updated successfully'
        ], 202);
    }

}
