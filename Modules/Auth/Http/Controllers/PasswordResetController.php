<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\User;
use App\Mail\SendEmailReset;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Modules\Auth\Http\Requests\UpdatePasswordRequest as UpdatePasswordRequest;
use Modules\Auth\Http\Requests\ResetPasswordRequest as ResetPasswordRequest;

class PasswordResetController extends Controller
{
    public function sendEmail(ResetPasswordRequest $request){
        if (!$this->validateEmail($request->email)) {
            return $this->failedResponse();
        }
        $this->send($request->email);
        return $this->successResponse();
    }

    public function send($email){
        $token = $this->createToken($email);
        Mail::to($email)->send(new SendEmailReset($token, $email));
    }

    public function createToken($email){
        $oldToken = DB::table('password_resets')->where('email', $email)->first();
        if ($oldToken){
            return $oldToken->token;
        }

        $token = Str::random(40);
        $this->saveToken($token, $email);
        return $token;
    }

    public function saveToken($token, $email){
//        dd($token, $email);

      return DB::table('password_resets')->insert([
            'email' => $email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

    }

    public function validateEmail($email){
        return !!User::where('email', $email)->first();
    }

    public function failedResponse(){
        return response()->json([
            'error' => 'Email does not found!'
        ], 404);
    }

    public function successResponse(){
        return response()->json([
            'data' => 'Reset email has been sent. Please check your email!'
        ], 200);
    }
}
