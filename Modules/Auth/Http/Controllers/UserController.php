<?php

namespace Modules\Auth\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Http\Requests\UpdateUserRequest as UpdateUserRequest;
use Symfony\Component\Console\Input\Input;

class UserController extends Controller
{

public function userProfile()
    {
        try {
            if (auth()->user()->id) {
                return response()->json([
                    'data' => auth()->user()
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }

    // update client details
    public function updateUserDetails(UpdateUserRequest $request){

        $id = Auth::user()->id;
        $user = User::find($id);
        $data = [
            'firstname' => $request->firstname ?? $user->firstname,
            'lastname' => $request->lastname ?? $user->lastname,
            'email' => $request->email ?? $user->email,
            'city' => $request->city ?? $user->city,
            'state' => $request->state ?? $user->state,
            'address1' => $request->address ?? $user->address,
            'country' => $request->country ?? $user->country,
            'postcode' => $request->postcode ?? $user->postcode,
            'phonenumber' => $request->phonenumber ?? $user->phonenumber,
        ];
        $user->update($data);
        return $user;
    }
}
