<?php

namespace Modules\Auth\Http\Controllers;

use App\Models\State;
use Illuminate\Routing\Controller;

class StateController extends Controller
{
    //
    public function allStates(){
        try {
            return State::all();
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getCitiesByStateId($id){
        try {
            $state = State::findOrFail($id);
            return $state->cities;
        } catch (\Exception $e) {
            return response()->json(['error' => trans('message.notFound')]);
        }
    }
}
