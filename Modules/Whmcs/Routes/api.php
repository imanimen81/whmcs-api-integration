<?php

use Illuminate\Support\Facades\Route;
use Modules\Whmcs\Http\Controllers\Integration\RegisterController;
use Modules\Whmcs\Http\Controllers\Integration\TicketController;
use Modules\Whmcs\Http\Controllers\InvoiceController;
use Modules\Whmcs\Http\Controllers\NewsController;
use Modules\Whmcs\Http\Controllers\OrderController;
use Modules\Whmcs\Http\Controllers\ProductController;
use Modules\Whmcs\Http\Controllers\TicketsController;
use Modules\Whmcs\Http\Controllers\WalletController;
use Modules\Whmcs\Http\Controllers\Integration\OrderController as IntegrationOrderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



/* Integration Route */
Route::group(['prefix' => 'integration', 'middleware' => ['localization', 'auth.apikey']], function ($router){

    Route::post('/register', [RegisterController::class, 'register']);
    // Tickets
    Route::post('/tickets', [TicketController::class, 'Tickets']);
    Route::post('/tickets/{id}', [TicketController::class, 'TicketShow']);
    Route::post('/ticket/status', [TicketController::class, 'TicketCount']);
    Route::post('/ticket/open', [TicketController::class, 'TicketOpen']);
    Route::post('/ticket/reply', [TicketController::class, 'TicketReply']);
    Route::post('/ticket/close', [TicketController::class, 'TicketClose']);
    Route::post('/ticket/status/{status}', [TicketController::class, 'GetTicketsByStatus']);

    // contact us
    Route::post('/contact-us/open', [TicketController::class, 'OpenContactUs']);
    Route::post('/contact-us', [TicketController::class, 'ContactUsGet']);
    Route::post('/contact-us/reply', [TicketController::class, 'ContactUsReply']);

    // Orders
    Route::post('/order/add', [IntegrationOrderController::class, 'OrderAdd']);
    Route::post('/orders', [IntegrationOrderController::class, 'GetOrders']);

    // Invoices
    Route::get('/invoices', [IntegrationInvoiceController::class, 'GetInvoices']);

});

/* None Authenticated User Route */
Route::group(['prefix' => '', 'middleware' => ['auth.apikey', 'localization']], function ($router) {
    // contact us
    Route::get('/contact-us', [TicketsController::class, 'ContactUsGet']); // get
    Route::post('/contact-us/open', [TicketsController::class, 'ContactUs']); // open contact us
    Route::post('/contact-us/reply', [TicketsController::class, 'ContactUsReply']); // reply contact us

     // products
    Route::get('/services', [ProductController::class, 'GetProduct']);
    Route::get('/services/{id}', [ProductController::class, 'GetProductID']);

    Route::get('/news', [NewsController::class, 'GetNews']);// get news
    Route::get('/news/{id}', [NewsController::class, 'NewsShow']); // show news

});


Route::group(['prefix' => 'client', 'middleware' => ['localization', 'auth.apikey']], function ($router){

    // tickets
    Route::get('/ticket/departments', [TicketsController::class, 'SupportDepartmentsList']); // ticket departments
    Route::get('/tickets', [TicketsController::class, 'TicketsList']);      // all user ticket
    Route::get('/tickets/{id}', [TicketsController::class, 'TicketShow']);  // show ticket
    Route::post('/ticket/open', [TicketsController::class, 'TicketOpen']);  // open ticket
    Route::post('/ticket/reply', [TicketsController::class, 'TicketReply']);// reply ticket
    Route::post('/ticket/close', [TicketsController::class, 'TicketClose']);// close ticket
    Route::get('/ticket/status', [TicketsController::class, 'TicketCount']);     // ticket count
    /* {status} -> open|closed|answered|customer-reply */
    Route::get('/ticket/status/{status}', [TicketsController::class, 'GetTicketsByStatus']);

    // news
    Route::get('/news', [NewsController::class, 'GetNews']);// get news
    Route::get('/news/{id}', [NewsController::class, 'NewsShow']); // show news

    // orders
    Route::post('/order/add', [OrderController::class, 'OrderAdd']);
    Route::get('/orders', [OrderController::class, 'GetOrders']);
    Route::get('/order/status/{status}', [OrderController::class, 'statusOrder']); // pending active fraud cancelled
    Route::get('/orders/{id}', [OrderController::class, 'OrderShow']);


    // products
    Route::get('/services', [ProductController::class, 'ClientProduct']); // client products all
    Route::get('/services/addons', [ProductController::class,'GetAddons']); // client addons
    Route::get('/services/{id}', [ProductController::class, 'ProductShow']); // single product

    // invoice
    Route::get('/invoices', [InvoiceController::class,'Invoices']); // get invoices
    Route::get('/invoices/{id}', [InvoiceController::class, 'ShowInvoice']);
    Route::get('/invoice/{status}', [InvoiceController::class, 'InvoiceStatus']);

    // payment 1
    Route::get('/payment-test/{id}', [InvoiceController::class, 'PayInvoice']);
    Route::get('/callback/bank', [InvoiceController::class, 'verify']);

    // add amount in invoice for wallet
    Route::post('/wallet/charge', [WalletController::class, 'ChargeWallet']); // charge wallet(add credit)
    Route::get('/wallet', [WalletController::class, 'GetWallet']); // get wallet
});
