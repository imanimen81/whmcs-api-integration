<?php

namespace Modules\Whmcs\Http\Controllers;


use Illuminate\Http\Request;

use Illuminate\Routing\Controller;
use Modules\Whmcs\Http\Traits\Connection;


class UserController extends Controller
{
   use Connection;

   // get all users
    public function GetAllUsers(Request $request){
       $search = $request->search;
       $getAlluser = $this->GetUsers($search);
       return json_decode($getAlluser);
    }
}
