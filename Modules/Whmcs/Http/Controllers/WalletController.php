<?php

namespace Modules\Whmcs\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Whmcs\Http\Requests\ChargeWalletRequest as ChargeWalletRequest;
use Modules\Whmcs\Http\Traits\Connection;
use Modules\Whmcs\Http\Traits\InvoicePay;

class WalletController extends Controller
{
    use InvoicePay;
    use Connection;

    public function ChargeWallet(ChargeWalletRequest $request){
        $clientId = auth()->user()->client_id;
        $amount = $request->input('itemamount1');
        $credit = $this->addCredit($clientId, $amount);
        $walletCharge = $this->AddClientCredit($clientId, $amount);
        return response()->json([
           'walletCharge' => $walletCharge,
           'credit' => $credit
        ]);
    }

    public function GetWallet(){
        $clientId = auth()->user()->client_id;
        $wallet = $this->GetClientCredit($clientId);
        return $this->connectionCheck($wallet);
    }
}
