<?php

namespace Modules\Whmcs\Http\Controllers;


use Illuminate\Routing\Controller;
use Modules\Whmcs\Http\Requests\AddOrderRequest as AddOrderRequest;
use Modules\Whmcs\Http\Traits\Connection;
use Modules\Whmcs\Http\Traits\InvoicePay;
use Modules\Whmcs\Http\Traits\Order;
use Modules\Whmcs\Http\Traits\Product;


class OrderController extends Controller
{
    use Order;
    use InvoicePay;
    use Product;
    use Connection;

    public function __construct()
    {
      $this->middleware('auth.apikey');
    }


    // add order
    public function OrderAdd(AddOrderRequest $request){
        try {
            $client_id = auth()->user()->client_id;
            $pid = $request->input('pid');
            $paymentmethod = $request->input('paymentmethod');
            $billingcycle = $request->input('billingcycle');
            $addorder = $this->Addorder($client_id, $pid, $billingcycle, $paymentmethod);
            return $this->connectionCheck($addorder);
        } catch(\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage()
              ], 500);
        }

    }


    // get orders
    public function GetOrders(){
        try {
            $client_id = auth()->user()->client_id;
            $getorder = $this->GetOderByClientId($client_id);
            return $this->connectionCheck($getorder);
        } catch (\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage()
              ], 500);
        }
    }

    // get order
   public function OrderShow($id){
       try {
           $response = $this->SingleOrder($id);
           return $this->connectionCheck($response);
       } catch (\Exception $e) {
           return response()->json([
               'error' => trans('message.OrderNotFound')
           ], 404);
       }
   }

    // get order status
    public function statusOrder($status){
        try {
            $clientid = auth()->user()->client_id;
            $orderstatus = $this->GetOrderStatuses($clientid, $status);
            return $this->connectionCheck($orderstatus);
        } catch (\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage()
              ], 500);
        }
    }
}
