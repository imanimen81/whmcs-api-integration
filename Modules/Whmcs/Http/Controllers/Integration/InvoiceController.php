<?php

namespace Modules\Whmcs\Http\Controllers\Integration;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Whmcs\Http\Traits\Connection;
use Modules\Whmcs\Http\Traits\InvoicePay;
use Modules\Whmcs\Http\Traits\Order;

class InvoiceController extends Controller
{
    use Connection, InvoicePay, Order;
    public function GetInvoices(Request $request){
        $client_id = $request->input('client_id');
        $response = $this->GetInvoices($client_id);
        return $this->connectionCheck($response);
    }

}
