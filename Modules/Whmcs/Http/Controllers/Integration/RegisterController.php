<?php

namespace Modules\Whmcs\Http\Controllers\Integration;


use Illuminate\Routing\Controller;
use Modules\Whmcs\Http\Requests\WhmRegisterRequest as WhmRegisterRequest;
use Modules\Whmcs\Http\Traits\Client;
use Modules\Whmcs\Http\Traits\Connection;


class RegisterController extends Controller
{
    use Client;
    use Connection;

    public function __construct()
    {
        $this->middleware('auth.apikey');
    }

    // register
    public function register(WhmRegisterRequest $request){
            $data = [
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'email' => $request->input('email'),
                'city' => $request->input('city'),
                'state' => $request->input('state'),
                'country' => $request->input('country'),
                'postcode' => $request->input('postcode'),
                'phonenumber' => $request->input('phonenumber'),
                'address1' => $request->input('address'),
                'password2' => $request->input('password')
            ];
            $response = $this->RegisterClient($data);
            if ($response->result == 'success'){
                return response()->json([
                    'data' => $response,
                ]);
            } elseif ($response->result == 'error'){
                return response()->json([
                    'error' => $response,
                ]);
            }
    }
}
