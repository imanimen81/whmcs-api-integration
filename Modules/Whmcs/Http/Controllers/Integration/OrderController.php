<?php

namespace Modules\Whmcs\Http\Controllers\Integration;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Whmcs\Http\Requests\AddOrderRequest as AddOrderRequest;
use Modules\Whmcs\Http\Traits\Connection;
use Modules\Whmcs\Http\Traits\Order;

class OrderController extends Controller
{
    use Connection, Order;

    public function OrderAdd(Request $request){
        try {
            $client_id = $request->input('client_id');
            $pid = $request->input('pid');
            $billingCycle = $request->input('billingcycle');
            $paymentMethod = $request->input('paymentmethod');
            $response = $this->AddOrder($client_id, $pid, $billingCycle, $paymentMethod);
            return $this->connectionCheck($response);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function GetOrders(Request $request){
        try {
            $client_id = $request->input('client_id');
            $getorder = $this->GetOderByClientId($client_id);
            return $this->connectionCheck($getorder);
        } catch (\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage()
            ], 500);
        }
    }
}
