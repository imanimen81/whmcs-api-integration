<?php

namespace Modules\Whmcs\Http\Controllers\Integration;


use Illuminate\Routing\Controller;
use Modules\Whmcs\Http\Requests\CloseTicketRequest as CloseTicketRequest;
use Modules\Whmcs\Http\Requests\ContactUsReplyRequest as ContactUsReplyRequest;
use Modules\Whmcs\Http\Requests\ContactUsRequest as ContactUsRequest;
use Modules\Whmcs\Http\Requests\GetContactUsRequest as GetContactUsRequest;
use Modules\Whmcs\Http\Requests\OpenTicketRequest as OpenTicketRequest;
use Modules\Whmcs\Http\Requests\ReplyTicketRequest as ReplyTicketRequest;
use Modules\Whmcs\Http\Traits\Connection;
use Modules\Whmcs\Http\Traits\Support;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    use Support;
    use Connection;

    public function __construct()
    {
      $this->middleware('auth.apikey');
    }

    // open ticket
    public function TicketOpen(OpenTicketRequest $request){
        try {
            $client_id = $request->input('client_id');
            $deptid = $request->input('deptid');
            $subject = $request->input('subject');
            $priority = $request->input('priority');
            $message = $request->input('message');
            $service_id = $request->input('serviceid');
            $openTicket = $this->OpenSupportTicket($client_id, $deptid, $subject, $priority, $message, $service_id);
            return $this->connectionCheck($openTicket);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }

    // get ticket
    public function Tickets(Request $request){
        try{
            $client_id = $request->input('client_id');
            $response = $this->GetTickets($client_id);
            return $this->connectionCheck($response);
        } catch(\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage(),
              ], 500);
        }
    }

    // show ticket
    public function TicketShow(Request $request, $id){
        try {

	
	    $client_id = $request->input('client_id');
            $ticket = $this->GetSupportTicketById($id);
            $user_in_ticket = $ticket->userid;
            if ($client_id == $user_in_ticket) {
            	return $this->connectionCheck($ticket);
            } else {
                return response()->json(['error' => trans('message.TicketAccess')], 403);
            }
        } catch (\Exception $e){
            return response()->json(['error' => trans('message.TicketNotFound')], 404);
        }
    }

    // close ticket
    public function TicketClose(CloseTicketRequest $request){
        try{
            $client_id = $request->input('client_id');
            $ticket_id = $request->input('ticketid');
            $closeTicket = $this->CloseTicket($client_id, $ticket_id);
            return $this->connectionCheck($closeTicket);
        } catch(\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage(),
              ], 500);
        }
    }


    // reply
    public function TicketReply(ReplyTicketRequest $request){
        try{
            $client_id = $request->input('client_id');
            $ticket_id = $request->input('ticketid');
            $message = $request->input('message');
            $ticketReply = $this->AddTicketReply($client_id, $ticket_id, $message);
            return $this->connectionCheck($ticketReply);
        } catch(\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage(),
              ], 500);
        }
    }


    // ticket count
    public function TicketCount(Request $request){
        try{
            $client_id = $request->input('client_id');
            $ticket_answered = $this->GetCountSupportTicketsByStatus($client_id,'answered');
            $ticket_reply = $this->GetCountSupportTicketsByStatus($client_id,'Customer-Reply');
            $ticket_active = $this->GetCountSupportTicketsByStatus($client_id,'Active');
            $ticket_open = $this->GetCountSupportTicketsByStatus($client_id,'Open');
            $ticket_closed = $this->GetCountSupportTicketsByStatus($client_id,'Closed');
            $response = [
                'answered'=> $ticket_answered->totalresults,
                'customer_reply'=>$ticket_reply-> totalresults,
                'active'=>$ticket_active-> totalresults,
                'open'=>$ticket_open-> totalresults,
                'closed'=>$ticket_closed-> totalresults,
            ];
            return response()->json($response);
        } catch(\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage(),
              ], 500);
        }

    }

        /*  filter tickets by status */
        public function GetTicketsByStatus(Request $request, $status){
            try {
                $user_id = $request->input('client_id');
                $res = $this->GetCountSupportTicketsByStatus($user_id, $status);
                return $this->connectionCheck($res);
            } catch(\Exception $e){
                return response()->json([
                   'error' => $e->getMessage()
                ]);
            }
        }
         /*  end filter tickets by status */

    // open contact us
    public function OpenContactUs(ContactUsRequest $request){
        try {
            $name = $request->input('name');
            $subject = $request->input('subject');
            $message = $request->input('message');
            $email = $request->input('email');
            $response = $this->SupportContactUs($name, $email, $message, $subject);
            return $this->connectionCheck($response);
        } catch (\Exception $e){
            return response([
                'data' => $e->getMessage()
            ],500);
        }
    }

    // contact us get
    public function ContactUsGet(GetContactUsRequest $request){
        try {
            $email = $request->input('email');
            $response = $this->GetContactUs($email);
            return $this->connectionCheck($response);
        } catch (\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage()
            ], 500);
        }
    }

    // contact us reply
    public function ContactUsReply(ContactUsReplyRequest $request){
        try {
            $ticket_id = $request->input('ticket_id');
            $email = $request->input('email');
            $name = $request->input('name');
            $message = $request->input('message');
            $response = $this->ReplyContactUs($ticket_id, $message, $email, $name);
            return $this->connectionCheck($response);
        } catch (\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage()
            ], 500);
        }
    }
}
