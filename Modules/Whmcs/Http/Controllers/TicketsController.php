<?php

namespace Modules\Whmcs\Http\Controllers;


use Illuminate\Routing\Controller;
use Modules\Whmcs\Http\Requests\CloseTicketRequest as CloseTicketRequest;
use Modules\Whmcs\Http\Requests\ContactUsReplyRequest as ContactUsReplyRequest;
use Modules\Whmcs\Http\Requests\OpenTicketRequest as OpenTicketRequest;
use Modules\Whmcs\Http\Requests\ReplyTicketRequest as ReplyTicketRequest;
use Modules\Whmcs\Http\Traits\Client;
use Modules\Whmcs\Http\Traits\Connection;
use Modules\Whmcs\Http\Traits\Support;
use Modules\Whmcs\Http\Requests\ContactUsRequest as ContactUsRequest;
use Modules\Whmcs\Http\Requests\GetContactUsRequest as GetContactUsRequest;

class TicketsController extends Controller
{

  use Connection;
  use Support;
  use Client;

  public function __construct()
  {
      $this->middleware('auth.apikey');
  }


    // support departments
    public function SupportDepartmentsList(){
      try {
          $departments = $this->GetSupportDepartments();
          return $this->connectionCheck($departments);
      } catch (\Exception $e) {
          return response()->json(['error' => $e->getMessage()], 500);
      }
    }


    // get tickets
    public function TicketsList(){
        try {
            $client_id = auth()->user()->client_id;
            $res = $this->GetTickets($client_id);
            return $this->connectionCheck($res);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }

    // get ticket by id
    public function TicketShow($id){
        try {
        $ticket = $this->GetSupportTicketById($id);
        $userInTicket = $ticket->userid;
        $client_id = auth()->user()->client_id;
        if ($client_id == $userInTicket) {
            return $this->connectionCheck($ticket);
        } else {
            return response()->json([
                'error' => trans('message.TicketAccess')
                ], 403);
        }
    } catch (\Exception $e){
        $data = [
            'message' => trans('message.TicketNotFound')
        ];
        return response()->json(['error' => trans('message.TicketNotFound')], 404);
    }
}


  // ticket count
       public function TicketCount()
    {
        try {
            $client_id = auth()->user()->client_id;
            $ticket_ans = $this->GetCountSupportTicketsByStatus($client_id, 'answered');
            $ticket_cr = $this->GetCountSupportTicketsByStatus($client_id,'Customer-Reply');
            $ticket_open = $this->GetCountSupportTicketsByStatus($client_id,'Open');
            $ticket_closed = $this->GetCountSupportTicketsByStatus($client_id,'closed');
            $response = [
                'answered'=> $ticket_ans->totalresults,
                'customer_reply'=> $ticket_cr->totalresults,
                'open'=> $ticket_open->totalresults,
                'closed'=> $ticket_closed->totalresults,
            ];
            return response()->json(['data' => $response], 200);

        } catch (\Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }


    // open ticket
    public function TicketOpen(OpenTicketRequest $request){
        try {
            $client_id = auth()->user()->client_id;
            $deptid = $request->input('deptid');
            $subject = $request->input('subject');
            $priority = $request->input('priority');
            $message = $request->input('message');
            $service_id = $request->input('serviceid');
            $openTicket = $this->OpenSupportTicket($client_id, $deptid, $subject, $priority, $message, $service_id);
            return $this->connectionCheck($openTicket);
        } catch (\Exception $e){
            return response([
                'error' =>  $e->getMessage(),
            ], 500);
        }
    }

    //  ticket reply
    public function TicketReply(ReplyTicketRequest $request){
        try {
            $client_id = auth()->user()->client_id;
            $ticket_id = $request->input('ticketid');
            $message = $request->input('message');
            $ticketReply = $this->AddTicketReply($client_id, $ticket_id, $message);
            return $this->connectionCheck($ticketReply);
        } catch (\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage()
            ], 500);
        }
    }

    /*  filter tickets by status */
    public function GetTicketsByStatus($status){
        try {
            $user_id = auth()->user()->client_id;
            $res = $this->GetCountSupportTicketsByStatus($user_id, $status);
            return $this->connectionCheck($res);
        } catch (\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage()
            ], 500);
        }
    }



    // close ticket
    public function TicketClose(CloseTicketRequest $request){
        try {
            $client_id = auth()->user()->client_id;
            $ticket_id = $request->input('ticketid');
            $closeTicket = $this->CloseTicket($client_id, $ticket_id);
            return $this->connectionCheck($closeTicket);
        } catch (\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage()
            ], 500);
        }
    }


    // contact us send
    public function ContactUs(ContactUsRequest $request){
        try {
            $name = $request->input('name');
            $email = $request->input('email');
            $subject = $request->input('subject');
            $message = $request->input('message');
            $response = $this->SupportContactUs($name, $email, $subject, $message);
            return $this->connectionCheck($response);
        } catch (\Exception $e) {
            return response()->json([
                'error' =>  $e->getMessage()
            ], 500);
        }
    }

    // contact us get
    public function ContactUsGet(GetContactUsRequest $request){
        try {
            $email = $request->input('email');
            $response = $this->GetContactUs($email);
            return $this->connectionCheck($response);
        } catch (\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage()
            ], 500);
        }
    }


    // contact us reply
    public function ContactUsReply(ContactUsReplyRequest $request){
        try {
            $ticket_id = $request->input('ticket_id');
            $email = $request->input('email');
            $name = $request->input('name');
            $message = $request->input('message');
            $response = $this->ReplyContactUs($ticket_id, $message, $email, $name);
            return $this->connectionCheck($response);
        } catch (\Exception $e){
            return response()->json([
                'error' =>  $e->getMessage()
            ], 500);
        }
    }
}
