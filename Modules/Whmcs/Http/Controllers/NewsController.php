<?php

namespace Modules\Whmcs\Http\Controllers;


use Illuminate\Routing\Controller;
use Modules\Whmcs\Http\Traits\Connection;
use Modules\Whmcs\Http\Traits\News;
class NewsController extends Controller
{
    use Connection;
    use News;

    public function __construct()
    {
      $this->middleware('auth.apikey');
    }


    // get news
    public function GetNews(){
        try {
            $announcement = $this->NewsGet();
            return response([
                'data' => $announcement->announcements->announcement,
            ], 200);
        } catch(\Exception $e){
            return response()->json([
                'error' => trans('message.SomethingWentWrong')
            ], 500);
        }
    }

    // single news
    public function NewsShow($id){
        try {
            $response = $this->GetNewsId($id);
            return response([
                'data' => $response,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => trans('message.NewsNotFound')
            ], 404);
        }
    }
}
