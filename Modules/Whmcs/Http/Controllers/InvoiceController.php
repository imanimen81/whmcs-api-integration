<?php

namespace Modules\Whmcs\Http\Controllers;



use Illuminate\Auth\Access\Gate;
use Illuminate\Routing\Controller;
use Larabookir\Gateway\Zarinpal\Zarinpal;
use Modules\Whmcs\Http\Traits\Connection;
use Larabookir\Gateway\Gateway;
use Larabookir\Gateway\Exceptions\InvalidRequestException;
use Larabookir\Gateway\Exceptions\NotFoundTransactionException;
use Larabookir\Gateway\Exceptions\PortNotFoundException;
use Larabookir\Gateway\Exceptions\RetryException;
use Modules\Whmcs\Http\Traits\Order;
use App\Models\InvoiceConnectTransaction;
use Modules\Whmcs\Http\Requests\ChargeWalletRequest as ChargeWalletRequest;
use Modules\Whmcs\Http\Requests\CreateInvoiceRequest as CreateInvoiceRequest;
use Modules\Whmcs\Http\Requests\AddTransactionRequest as AddTransactionRequest;
use Modules\Whmcs\Http\Traits\InvoicePay;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment as PaymentFacade;

class InvoiceController extends Controller
{
    use Connection;
    use InvoicePay;
    use Order;


    public function Invoices()
    {
        $client_id = auth()->user()->client_id;
        $response = $this->GetInvoices($client_id);
        return response([
            'data' => $response
        ], 200);
    }

    // single invoice
    public function ShowInvoice($id)
    {
        try {
            $response = $this->GetInvoice($id);
            return response([
                'data' => $response
            ], 200);
        } catch (\Exception $e) {
            $data = [
                'message' => trans('message.InvoiceNotFound')
            ];
            return response()->json($data, 404);
        }
    }

    // filter by status
    public function InvoiceStatus($status)
    {
        $client_id = auth()->user()->client_id;
        $response = $this->GetInvoiceByStatus($client_id, $status);
        return $this->connectionCheck($response);
    }

    // get transactions
    public function GetTransactions()
    {
        $client_id = auth()->user()->client_id;
        $response = $this->GetClientTrasaction($client_id);
        return response([
            'data' => $response->transactions
        ], 200);
    }


    // Payment Gateway
    public function PayInvoice($id)
    {
        try {
            $client_id = auth()->user()->client_id;
            $invoice = $this->GetInvoiceForPayment($id);
            $price = $invoice->total;
            $invoice_id = $invoice->invoiceid;
            $check = InvoiceConnectTransaction::all()->where('invoice_id', $invoice_id)->first();
                $gateway = Gateway::zarinpal();
                $gateway->setCallback(url('/api/client/callback/bank'));
                $gateway->price($price)->ready();
                $refId = $gateway->refId();
                $transID = $gateway->transactionId();
                $note = $invoice->notes;
                if ($note == 'credit') {
                    $credit = 1;
                } else {
                    $credit = 0;
                }
                $order_id = $this->GetClientOrderId($client_id, $invoice_id);
                InvoiceConnectTransaction::create([
                    'order_id' => $order_id,
                    'transaction_id' => $transID,
                    'client_id' => $client_id,
                    'invoice_id' => $invoice_id,
                    'amountin' => round($invoice->total),
                    'credit' => $credit,
                ]);
                return $gateway->redirect();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    // verify payment
    public function verify()
    {
        try {
            $gateway = Gateway::verify();
            $trackingCode = $gateway->trackingCode();
            $refId = $gateway->refId();
            $transID = $gateway->transactionId();
            $cardNumber = $gateway->cardNumber();
            $order = InvoiceConnectTransaction::all()->where('transaction_id', $transID)->first();
            $order?->update(array('status'=>'paid'));
            $credit = $order->credit;
            $client_id = $order->client_id;
            $invoice_id = $order->invoice_id;
            $invoice_id = $order->invoice_id;
            $amountin = $order->amountin;
            $order_id = $order->order_id;
            $this->AddClientTransaction($client_id, $invoice_id, $transID, $amountin);
            $this->AcceptPendingOrder($order_id);
            $this->updateInvoice($invoice_id);
            $message = "Paid Successfully";
            return view('payment', ['message' => $message]);
        } catch (RetryException | PortNotFoundException | NotFoundTransactionException $e) {
            return view('payment', ['message' => $e->getMessage()]);
        } catch (InvalidRequestException $e) {
            return view('payment', ['message' => $e->getMessage()]);
        } catch (\Exception $e) {
            return view('payment', ['message' => $e->getMessage()]);
        }
    }
}
