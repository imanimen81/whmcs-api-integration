<?php

namespace Modules\Whmcs\Http\Controllers;


use Illuminate\Routing\Controller;
use Modules\Whmcs\Http\Traits\Connection;
use Modules\Whmcs\Http\Traits\Product;

class ProductController extends Controller
{
    use Connection;
    use Product;
    public function __construct()
    {
      $this->middleware('auth.apikey');
    }

    // products
    public function GetProduct(){
        try {
            $response = $this->GetAllProducts();
            return $this->connectionCheck($response);
        } catch (\Exception $e) {
            return response()->json([
                'error' => trans('message.SomethingWentWrong')
            ], 500);
        }
    }

    // product one id
    public function GetProductID($id){
        try {
            $product = $this->GetProductsById($id);
            return response()->json([
                'data' => $product
            ], 200);
        } catch (\Exception $e){
            return response()->json(['error' => trans('message.ServiceNotFound')],404);
        }
    }

    // get client product
    public function ClientProduct(){
        try {
            $client_id = auth()->user()->client_id;
            $response = $this->GetAllClientProducts($client_id);
            return $this->connectionCheck($response);
        } catch (\Exception $e) {
            return response()->json(['error' => trans('message.SomethingWentWrong')], 404);
        }
    }

    // client single product
    public function ProductShow($id){
        try {
            $client_id = auth()->user()->client_id;
            $response = $this->GetClientProducts($client_id, $id);
//            return $this->connectionCheck($response);
            return response([
                'data' => $response,
            ], 200);
        } catch (\Exception $e){
            $message = [
                'message' => trans('message.ServiceNotFound')
            ];
            return response()->json($message, 404);
        }
    }
    // client addons
    public function GetAddons(){
        $client_id = auth()->user()->client_id;
        return $this->GetClinetAddons($client_id);
    }

}
