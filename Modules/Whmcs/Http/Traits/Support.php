<?php

namespace Modules\Whmcs\Http\Traits;

trait Support
{
    use Connection;

    //  get tickets
    public function GetTickets($client_id){
        $action = 'GetTickets';
        $data = [
            'clientid' => $client_id
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return  json_decode($res);
    }


    // support departments
    public function GetSupportDepartments()
    {
        $action = 'GetSupportDepartments';
        $data = [
        'ignore_dept_assignments' => true
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }


    // tickets count
    public function GetCountSupportTicketsByStatus($user_id,$status){
        $action = 'GetTickets';
        $data = [
            'clientid'=>$user_id,
            'status'=> $status,
        ];
        $res =$this->SendRequest($action,'json',$data);
        return json_decode($res);
    }


    // open ticket
    public function OpenSupportTicket($clientid, $deptid, $subject,$priority,$message,$service_id){
        $action = 'OpenTicket';
        $data = [
          'clientid' => $clientid,
          'deptid' => $deptid,
          'subject' => $subject,
          'priority' => $priority,
          'message' => $message,
          'serviceid' => $service_id,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // get ticket attachment
    public function GetTicketAttachment($relatedid, $type, $index){
        $action = 'GetTicketAttachment';
        $data = [
            'relatedid' => $relatedid,
            'type' => $type,
            'index' => $index,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // Add Ticket Reply
    public function AddTicketReply($client_id, $ticket_id, $message){
        $action = 'AddTicketReply';
        $data = [
            'clientid' => $client_id,
            'ticketid' => $ticket_id,
            'message' => $message,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // close ticket
    public function CloseTicket($client_id, $ticket_id){
        $action = 'UpdateTicket';
        $data = [
            'userid' => $client_id,
            'ticketid' => $ticket_id,
            'status' => 'Closed',
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // contact us
    public function SupportContactUs($name, $email, $message, $subject){
        $action = 'OpenTicket';
        $data = [
            'name' => $name,
            'email' => $email,
            'deptid' => '2',
            'subject' => $subject,
            'message' => $message,
            'markdown' => true,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // get contact us
    public function GetContactUs($email){
        $action = "GetTickets";
        $data = [
            'email' => $email,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // reply contact us
    public function ReplyContactUs($ticket_id, $message, $email, $name){
        $action = 'AddTicketReply';
        $data = [
            'ticketid' => $ticket_id,
            'name'   => $name,
            'message' => $message,
            'email' => $email,
            'markdown' => true,
            'status' => 'Customer-Reply',
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // get ticket by id
    public function GetSupportTicketById($ticket_id)
    {
            $action = 'GetTicket';
            $data = ['ticketid' => $ticket_id];
            $res = $this->SendRequest($action, 'json', $data);
            return json_decode($res);
    }
}
