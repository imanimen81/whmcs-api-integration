<?php

namespace Modules\Whmcs\Http\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;

trait Connection
{
    public function SendRequest($action,$response_type,$data){
        try {
            $post_fields = array(
                'identifier' => config('whmcs.identifier'),
                'secret' => config('whmcs.secret'),
                'action' => $action,
                'responsetype' => $response_type,
            );

            $post_fields = array_merge($post_fields,$data);

            $client = new Client([
                'base_uri' => config('whmcs.url'),
                'timeout'  => 600,
            ]);


            $results = $client->request('POST', config('whmcs.url'), [
                'form_params' => $post_fields
            ]);

            return $results->getBody();
        }  catch (ServerException $e) {
            return $e->getMessage();
        } catch (GuzzleException $e) {
            return $e->getMessage();
        }
    }

    /*
test function
*/
    public function connectionCheck($res){
        if ($res->result == 'success') {
            return response()->json(['data' => $res], 200);
        } elseif ($res->result == 'error') {
            return response()->json(['error' => $res], 500);
        } else {
            return 'error';
        }
    }
}
