<?php

namespace Modules\Whmcs\Http\Traits;


use Illuminate\Support\Facades\Http;

trait Product
{

    use Client;

    public function GetAllProducts(){
        $action = 'GetProducts';
        $data = [
            'stats' => true
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }


    public function GetAllClientProducts($client_id){
        $action = 'GetClientsProducts';
        $data = [
            'clientid' => $client_id,
            'stats' => true
        ];
        $res = $this->SendRequest($action, 'json', $data);
        $response = json_decode($res);
        return $response;
    }

    // single product
    public function GetProductsById($product_id){
        $action = 'GetProducts';
        $data = [
            'stats' => true
        ];
        $res = $this->SendRequest($action, 'json', $data);
        $res = json_decode($res);

        foreach ($res->products->product as $item) {
            if ($item->pid == $product_id) {
                $response = $item;
            }
        }
        return $response;
    }

    public function GetProduct($client_id){
        $action = 'GetProducts';
        $data = [
            'clientid' => $client_id,
            'stats' => true,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        $respone = json_decode($res);
        return $respone->products;
    }




    // single product
    public function GetClientProducts($client_id, $product_id){
            $action = 'GetClientsProducts';
            $data = [
                'clientid' => $client_id,
                'stats' => true
            ];
            $res = $this->SendRequest($action, 'json', $data);
            $res = json_decode($res);

            foreach ($res->products->product as $item) {
                if ($item->id == $product_id) {
                    $response = $item;
                }
            }
            return $response;
    }

    // client addons
    public function GetClinetAddons($client_id){
        $action = 'GetClientsAddons';
        $data = [
            'clientid' => $client_id,
            'stats' => true
        ];
        $res = $this->SendRequest($action, 'json', $data);
        $res = json_decode($res);
        return $res;
    }
}
