<?php

namespace Modules\Whmcs\Http\Traits;

trait InvoicePay
{
    use Connection;


    public function InvoiceGen($client_id, $serviceids)
    {
        $action = 'GenInvoices';
        $data = [
            'clientid' => $client_id,
            'serviceids' => $serviceids
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    public function GetInvoices($client_id)
    {
        $action = 'GetInvoices';
        $data = [
            'userid' => $client_id
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    public function CreateInvoice($client_id, $itemamountx, $itemdescription1)
    {
        $action = 'CreateInvoice';
        $data = [
            'userid' => $client_id,
            'status' => 'unpaid',
            'sendinvoice' => '1',
            'itemamount1' => $itemamountx,
            'itemdescription1' => $itemdescription1,
            'paymentmethod' => 'idpay',
        ];
        $res = $this->SendRequest($action, 'json', $data);
        $response = json_decode($res);
        return $response;
    }

    // single invoice
    public function GetInvoice($invoice_id)
    {
        $action = 'GetInvoice';
        $data = [
            'invoiceid' => $invoice_id
        ];
        $res = $this->SendRequest($action, 'json', $data);
        $res = json_decode($res);
        $invoice_client_id = $res->userid;
        $client_id = auth()->user()->client_id;
        if ($client_id == $invoice_client_id) {
            return response()->json($res);
        } else {
            return response()->json(['error' => 'Not Found'], 403);
        }
    }

    public function GetInvoiceByDueData($client_id)
    {
        $action = 'GetInvoices';
        $data = [
            'userid' => $client_id,
            'status' => 'Unpaid'
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // filter invoice by status
    public function GetInvoiceByStatus($client_id, $status)
    {
        $action = 'GetInvoices';
        $data = [
            'userid' => $client_id,
            'status' => $status,
            'responsetype' => 'json',
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // add credit
    public function AddClientCredit($client_id, $itemamount1)
    {
        $action = 'CreateInvoice';
        $data = [
            'userid' => $client_id,
            'paymentmethod' => 'idpay',
            'itemdescription1' => 'Charge Wallet',
            'itemamount1' => $itemamount1,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // get client credit
    public function GetClientCredit($client_id)
    {
        $action = 'GetCredits';
        $data = [
            'clientid' => $client_id,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // add transaction
    public function AddClientTransaction($client_id, $transid, $invoiceid, $amountin)
    {
        $action = 'AddTransaction';
        $data = [
            'userid' => $client_id,
            'invoiceid' => $invoiceid,
            // 'description' => $description,
            'amountin' => $amountin,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // get transaction
    public function GetClientTrasaction($client_id)
    {
        $action = 'GetTransactions';
        $data = [
            'clientid' => $client_id,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        $response = json_decode($res);
        return $response;
    }

    // add credit
    public function addCredit($client_id, $amount){
        $action = 'AddCredit';
        $data = [
          'clientid' => $client_id,
          'amount'=> $amount,
          'description' => 'add credit to wallet',
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // get credit
    public function getCredit($clientid){
        $action = 'GetCredits';
        $data = ['clientid' => $clientid];
        $res = $this->SendRequest($action, 'json', $data);
        return  json_decode($res);
    }


    // get invoice for payment
    public function GetInvoiceForPayment($invoice_id)
    {
        $action = 'GetInvoice';
        $data = [
            'invoiceid' => $invoice_id,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        $res = json_decode($res);
        $invoice_client_id = $res->userid;
        $client_id = auth()->user()->client_id;
        if ($client_id == $invoice_client_id) {
            return $res;
        } else {
            return "Not Found In Invoices";
        }
    }


    // create client invoice
    public function CreateCreditInvoice($client_id, $itemamountx)
    {
        $action = 'CreateInvoice';
        $data = [
            'userid' => $client_id,
            'paymentmethod' => 'idpay',
            'notes' => 'credit',
            'itemdescription1' => 'Charge Wallet',
            'itemamount1' => $itemamountx,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }


    // on payment
    public function AcceptClientOrder($invoiceid, $transid)
    {
        $action = 'AddInvoicePayment';
        $data = [
            'invoiceid' => $invoiceid,
            'transid' => $transid,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // continue order
    public function AcceptPendingOrder($orderid)
    {
        $action = 'AcceptOrder';
        $data = [
            'orderid' => $orderid
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    public function updateInvoice($invoiceid){
        $action = 'UpdateInvoice';
        $data = [
            'invoiceid' => $invoiceid,
            'status' => 'Paid',
        ];
        $res = $this->SendRequest($action,'json',$data);
        return json_decode($res);
    }
}
