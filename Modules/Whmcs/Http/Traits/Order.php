<?php

namespace Modules\Whmcs\Http\Traits;

use Modules\Whmcs\Http\Traits\Connection;

trait Order {


    use Connection;


    // add order
    public function Addorder($client_id, $pid, $billingcycle, $paymentmethod){
       $action = 'AddOrder';
       $data = [
           'clientid' => $client_id,
           'pid' => array($pid),
           'billingcycle' => $billingcycle,
           'paymentmethod' => $paymentmethod,
       ];
       $res = $this->SendRequest($action, 'json', $data);
       return json_decode($res);
    }

    public function SingleOrder($orderid)
    {
        $action = 'GetOrders';
        $data = [
            'userid' => auth()->user()->client_id,
            'id' => $orderid,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }


    // get order by client id
    public function GetOderByClientId($clientid){
        $action = 'GetOrders';
        $data = [
            'userid' => $clientid,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // get clients order id
    public function GetClientOrderId($client_id, $invoice_id){
        $action = 'GetOrders';
        $data = [
            'userid' => $client_id,
        ];
        $response = $this->SendRequest($action,'json', $data);
        $res = json_decode($response);
//        dd($res);
        $order = $res->orders->order;

        return $order[0]->id;
//        $resf = 0;
//        foreach ($order as $item){
//            $test = $item->invoiceid;
////            if ($item->inoviceid == $invoice_id){
////                $resf = $item->id;
////            }
//        }
//        return $test;
    }


    // get order status
    public function GetOrderStatuses($clientid, $status){
        $action = 'GetOrders';
        $data = [
            'userid' => $clientid,
            'status' => $status
        ];
        $res = $this->SendRequest($action, 'json', $data);
        $response = json_decode($res);
        return $response;
    }


    // cnacel order
    public function CancelOrder($clientid, $orderid){
        $action = 'CancelOrder';
        $data = [
          'clientid' => $clientid,
          'orderid' => $orderid,
        ];
        $res = $this->SendRequest($action, 'json', $data);
        $response = json_decode($res);
        return $response;
    }

    // pending order
    public function PendOrder($client_id){
        $action = 'GetOrders';
        $data = [
            'clientid' => $client_id,
            'status' => 'Pending'
        ];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

}
