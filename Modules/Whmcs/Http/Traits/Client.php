<?php

namespace Modules\Whmcs\Http\Traits;

trait Client
{
    use Connection;

    public function EmailCheck($email){
        $action = 'GetClients';
        $data = [
            'search'=> $email
        ];
        $res =$this->SendRequest($action,'json',$data);
        $res = json_decode($res);
        $flag = false;
        if(isset($res->clients)){
            $flag = true;
        }
        return $flag;
    }


    public function GetClientIdForRegistration($email){
        $action = 'GetClients';
        $data = [
            'search'=> $email
        ];
        $res =$this->SendRequest($action,'json',$data);
        $res = json_decode($res);
        $res = $res->clients->client;
        return $res[0]->id;
    }


    public function GetClientProducts($user_id,$start){
        $action = 'GetClientsProducts';
        $data = [
            'clientid'=>$user_id,
            'stats'=>true,
            'limitstart'=> $start,
            'limitnum'=> 10
        ];
        $res =$this->SendRequest($action,'json',$data);
        $res = json_decode($res);
        return $res;

    }

    public function RegisterClient($data){
        $action = 'AddClient';
        $res =$this->SendRequest($action,'json',$data);
        $res = json_decode($res);
        return $res;
    }


}
