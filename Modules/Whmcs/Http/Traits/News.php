<?php

namespace Modules\Whmcs\Http\Traits;

trait News
{
    use Connection;

    // get news
    public function NewsGet()
    {
        $action = 'GetAnnouncements';
        $data = [''];
        $res = $this->SendRequest($action, 'json', $data);
        return json_decode($res);
    }

    // get news by id
    public function GetNewsId($annoucement_id)
    {
        $action = 'GetAnnouncements';
        $data = [
            'announcementid' => $annoucement_id
        ];
        $res = $this->SendRequest($action, 'json', $data);
        $res = json_decode($res);
        foreach ($res->announcements->announcement as $annoucement) {
            if ($annoucement->id == $annoucement_id) {
                $response = $annoucement;
            }
        }
        return $response;
    }
}

