<?php

namespace Modules\Whmcs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactUsReplyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticketid' => 'required',
            'email' => 'required|email',
            'name' => 'required|string',
            'message' => 'required|string|max:10000'
        ];
    }

    public function messages()
    {
        return [
            'ticketid.required' => trans('message.TicketIdRequired'),
            'name.required' => trans('message.NameRequired'),
            'email.required' => trans('message.EmailRequired'),
            'email.email' => trans('message.EmailInvalid'),
            'message.required' => trans('message.MessageRequired'),
            'message.string' => trans('message.MessageString')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
