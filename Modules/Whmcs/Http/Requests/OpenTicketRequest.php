<?php

namespace Modules\Whmcs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class OpenTicketRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'deptid' => 'required',
            'subject' => 'bail|required|string|max:255',
            'message' => 'bail|required|string|max:1000',
        ];
    }

    public function messages()
    {
        return  [
            'deptid.required' => trans('message.DeptRequired'),
            'subject.required' => trans('message.SubjectRequired'),
            'subject.string' => trans('message.SubjectString'),
            'message.required' => trans('message.MessageRequired'),
            'message.string' => trans('message.MessageString'),
            'message.max' => trans('message.MessageMax')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
