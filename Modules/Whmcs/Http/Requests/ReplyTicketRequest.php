<?php

namespace Modules\Whmcs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReplyTicketRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticketid' => 'required',
            'message' => 'bail|required|string|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'ticketid.required' => trans('message.TicketIdRequired'),
            'message.required' => trans('message.MessageRequired'),
            'message.string' => trans('message.MessageString'),
            'message.max' => trans('message.MessageMax')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
