<?php

namespace Modules\Whmcs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddOrderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pid' => 'required|integer',
            'paymentmethod' => 'required',
            'billingcycle' => 'required'
        ];
    }

    public function messages(){
        return [
            'pid.required' => trans('message.PidReq'),
            'paymentmethod.required' => trans('message.PayMethodReq'),
            'billingcycle.required' => trans('message.BillingReq')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
