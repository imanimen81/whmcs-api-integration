<?php

namespace Modules\Whmcs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddTransactionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amountin' => 'required',
            'invoiceid' => 'required',
            'description' => 'string'
        ];
    }

    public function message(){
        return [
            'amountin.required' => trans('message.AmounInReq'),
            'invoiceid.required' => trans('message.InvoiceIdReq'),
            'description.string' => trans('message.DescString')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
