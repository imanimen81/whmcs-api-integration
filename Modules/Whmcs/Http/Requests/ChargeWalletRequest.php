<?php

namespace Modules\Whmcs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChargeWalletRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'itemamount1' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'itemamount1.required' => trans('message.ItemAmountReq')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
