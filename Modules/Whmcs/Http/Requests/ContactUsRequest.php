<?php

namespace Modules\Whmcs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactUsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'subject' => 'required|string|max:255',
            'message' => 'required|string|max:1000'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => trans('message.NameRequired'),
            'email.required' => trans('message.EmailRequired'),
            'email.email' => trans('message.EmailInvalid'),
            'subject.required' => trans('message.SubjectRequired'),
            'message.required' => trans('message.MessageRequired'),
            'message.string' => trans('message.MessageString'),
            'subject.string' => trans('message.SubjectString'),
            'message.max' => trans('message.MessageMax')
        ];

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
