<?php

namespace Modules\Whmcs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WhmRegisterRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'bail|required|email|max:35|unique:users,email',
            'city' => 'required|string|max:255',
            'country' => 'required|between:0,3|regex:([A-Z])',
            'state' => 'required|string|max:255',
            'postcode' => 'bail|required|max:255|unique:users',
            'phonenumber' => 'bail|required|min:11|numeric|unique:users',
            'address1' => 'required|max:255',
            'password' => 'bail|required|min:6|confirmed',
        ];
    }
    public function messages(){
        return  [
            'email.required' => trans('auth.EmailRequired'),
            'email.email' => trans('auth.InvalidEmail'),
            "email.unique" => trans('auth.EmailExists'),
            'city.required' => trans('auth.CityRequired'),
            'address1.required' => trans('auth.AddrRequired'),
            'state.required' => trans('auth.StateRequired'),
            'country.required' => trans('auth.CountryRequired'),
            'phonenumber.required' => trans('auth.PhoneNumberRequired'),
            'phonenumber.unique' => trans('auth.PhoneNumberUnique'),
            'phonenumber.min' => trans('message.PhonenumberMin'),
            'postcode.unique' => trans('auth.postcodeUnique'),
            'postcode.required' => trans('auth.PostRequired'),
            'firstname.required' => trans('auth.FnameRequired'),
            'lastname.required' => trans('auth.LnameRequired'),
            'password.required' => trans('auth.RequiredPass'),
            'password.confirmed' => trans('auth.ConfirmPass'),
            'password.min' => trans('message.PasswordMin'),
            'country.regex' => trans('message.CountryRegex')
        ];
    }
}
