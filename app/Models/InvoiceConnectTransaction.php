<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class InvoiceConnectTransaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'transaction_id',
        'client_id',
        'invoice_id',
        'amountin',
        'credit',
    ];

    public function users()
    {
        return $this->belongsTo(\App\Models\User::class, 'client_id');
    }
}
