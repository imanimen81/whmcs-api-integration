<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Modules\Whmcs\Http\Traits\Connection;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    // use Client;
    use Connection;
    use HasFactory, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'city',
        'state',
        'country',
        'postcode',
        'address1',
        'email',
        'phonenumber',
        'password',
        'client_id',
        'token',
        'is_verified',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function RegisterClient($dataWhmcs){
        $action = 'AddClient';
        $res =$this->SendRequest($action,'json',$dataWhmcs);
        $response = json_decode($res);
        $result = $response->result;
        switch ($result) {
            case 'success':
                return $response->clientid;
                break;
            case 'error':
                $user = User::where('email', $dataWhmcs['email'])->first();
                $user?->delete();
                break;
            }
    }

    public function updateClientId($email, $clientId){
        $user = User::where('email', $email)->first();
        $user?->update(array('client_id' => $clientId));
    }

    public function register(array $data){
        try {
            $user = new User();
            $user->firstname = $data['firstname'];
            $user->lastname = $data['lastname'];
            $user->email = $data['email'];
            $user->address1 = $data['address1'];
            $user->postcode = $data['postcode'];
            $user->phonenumber = $data['phonenumber'];
            $user->city = $data['city'];
            $user->state = $data['state'];
            $user->country = $data['country'];
            $user->password =  Hash::make($data['password']);
            $user->save();
            return $user;
        } catch (\Exception $e){
            return $e->getMessage();
        }
    }


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function invoiceConnectTransaction()
    {
        return $this->hasMany(\App\Models\InvoiceConnectTransaction::class);
    }
}
