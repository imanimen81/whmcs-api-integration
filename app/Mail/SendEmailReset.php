<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmailReset extends Mailable
{
    use Queueable, SerializesModels;


    public $token;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $data)
    {
        $this->token = $token;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('passwordReset')->with([
            'token' => $this->token,
            'data' => $this->data
        ]);
    }
}
