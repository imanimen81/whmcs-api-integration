@component('mail::message')



@component('mail::button', ['url' => 'http://localhost:8000/reset-password?token='.$token])
Button Text
@endcomponent

    Thanks
    {{ config('app.name') }}
@endcomponent
