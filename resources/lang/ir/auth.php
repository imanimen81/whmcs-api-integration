<?php

return [
    'EmailExists' => 'ایمیل وارد شده در سیستم ثبت شده است.',
    'Registered' => 'ثبت نام شما با موفقیت انجام شد.',
    'EmailRequired' => 'ایمیل را وارد نکردید',
    'InvalidEmail' => 'ایمیل وارد شده معتبر نیست.',
    'Unauthorized' => 'مشخصات وارد شده صحیج نمیباشد.',
    'RequiredPass' => 'رمز عبور را وارد نکردید ',
    'FnameRequired' => 'نام را وارد نکردید',
    'LnameRequired' => 'نام خانوادگی را وارد نکردید',
    'CityRequired' => 'شهر را وارد نکردید',
    'AddrRequired' => 'آدرس را وارد نکردید',
    'StateRequired' => 'استان را وارد نکردید',
    'CountryRequired' => 'کشور را وارد نکردید',
    'PhoneNumberRequired' => 'شماره تماس را وارد نکردید',
    'PostRequired' => 'کد پستی را وارد نکردید',
    'PhonenumberExists' => 'شماره تماس وارد شده در سیستم ثبت شده است.',
    'ConfirmPass' => 'رمز عبور وارد شده یکسان نیست',
    'LogedOut' => 'شما با موفقیت از سیستم خارج شدید.',
    'PhoneNumberUnique' => 'شماره تماس وارد شده در سیستم ثبت شده است.',
    'postcodeUnique' => 'کد پستی وارد شده در سیستم ثبت شده است.',
];
