<?php

return [
    'TicketNotFound' => 'تیکت یافت نشد',
    'TicketAccess' => 'این تیکت به شما تعلق ندارد',
    'ServiceNotFound' => 'سرویس یافت نشد',
    'NewsNotFound' => 'خبر یافت نشد',
    'InvoiceNotFound' => 'فاکتور یافت نشد',
    'OrderNotFound' => 'سفارش یافت نشد',
    'SomethingWentWrong' => ' مشکلی پیش آمده است. مجدد تلاش کنید!',
    'MessageRequired' => 'پیام را وارد کنید',
    'SubjectRequired' => 'موضوع را وارد کنید',
    'DeptReq' => 'دپارتمان تیکت را وارد کنید',
    'SubjectString' => 'موضوع فقط باید شامل کاراکترهای انگلیسی و فارسی و اعداد باشد',
    'MessageString' => 'پیام فقط باید شامل کاراکترهای انگلیسی و فارسی و اعداد باشد',
    'TicketIdRequired' => 'شناسه تیکت را وارد کنید',
    'EmailRequired' => 'ایمیل را وارد کنید',
    'EmailInvalid' => 'ایمیل وارد شده معتبر نیست',
    'NameRequired' => 'نام را وارد کنید',
    'PasswordMin' => 'رمز عبور حداقل باید 6 کاراکتر باشد',
    'EmailNotFound' => 'ایمیل وارد شده موجود نیست',
    'CountryRegex' => 'کد کشور باید دو حرفی و با حروف بزرگ انگلیسی باشد',
    'PidReq' => 'آی دی محصول را وارد نکردید!',
    'PayMethodReq' => 'روش پرداختی وارد شده نامعتبر است. idpay , PayPal مجاز میباشند!',
    'BillingReq' => 'دوره پرداخت وارد شده نامعتبر است. ماهانه، سالانه مجاز میباشند.',
    'OrderIdRrq' => 'آی دی سفارش نامعتبر است',
    'ItemAmountReq' => 'مقدار را وارد نکردید!',
    'ItemDescReq' => 'توضیحات فاکتور را وارد نکردید!',
    'InvoiceIdReq' => 'َای دی فاکتور را وارد نکردید',
    'DescString' => 'پیام فقط باید شامل کاراکترهای انگلیسی و فارسی و اعداد باشد',
    'PhonenumberMin' => 'شماره همراه باید حداقل شامل 11 عدد باشد',
    'phonenumberNum' => 'شماره همراه وارد شده فقط باید شامل اعداد باشد',
    'MessageMax' => 'متن پیام نمیتواند بیشتر از 1000 کلمه باشد',
    'notFound' => 'موردی یافت نشد',
    'DeptRequired' => 'دپارتمان را وارد نکردید',
    'DeptRequired2' => 'دپارتمان را وارد نکردید',

];
