<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'EmailExists' => 'This email address is already registered.',
    'Registered' => 'You have been registered successfully.',
    'EmailRequired' => 'The email field is required.',
    'InvalidEmail' => 'The email address is invalid.',
    'Unauthorized' => 'Unauthorized.',
    'RequiredPass' => 'The password field is required.',
    'ConfirmPass' => 'The password confirmation does not match.',
    'FnameRequired' => 'The first name field is required.',
    'LnameRequired' => 'The last name field is required.',
    'CityRequired' => 'The city field is required.',
    'AddrRequired' => 'The address field is required.',
    'StateRequired' => 'The state field is required.',
    'CountryRequired' => 'The country field is required.',
    'PhoneNumberRequired' => 'The phone number field is required.',
    'PhoneNumberUnique' => 'The phone number is already registered.',
    'PostRequired' => 'The post code field is required.',
    'PhonenumberExists' => 'This phone number is already registered.',
    'LogedOut' => 'You have been logged out successfully.',
    'postcodeUnique' => 'This post code is already registered.',


];
